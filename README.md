# Admin

## Előfeltételek

Telepíteni kell a következő modulokat.:
- https://gitlab.com/molitor/menu
- https://gitlab.com/molitor/data_table

## Telepítés

### Provider regisztrálása
config/app.php
```php
'providers' => ServiceProvider::defaultProviders()->merge([
    /*
    * Package Service Providers...
    */
    \Molitor\Admin\Providers\AdminServiceProvider::class,
])->toArray(),
```