<?php


namespace Molitor\Admin\Http\Controllers\Admin;


use Molitor\Menu\Facades\Menu;

class AdminController extends BaseAdminController
{
    public function index()
    {
        return view('admin::index');
    }

    public function settings()
    {
        return view('admin::settings', [
            'menu' => Menu::adminSetting(),
        ]);
    }
}
