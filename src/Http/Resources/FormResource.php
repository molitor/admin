<?php

namespace Molitor\Admin\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

abstract class FormResource extends JsonResource
{
    private ?string $redirect = null;

    private ?string $successMessage = null;

    public function __construct($resource = null)
    {
        $this->resource = $resource;
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $array = [];

        if ($this->resource) {
            $array['formValues'] = $this->getFormValues($request);
            $array['formData'] = $this->getFormData($request);
        }

        if ($this->redirect) {
            $array['redirect'] = $this->redirect;
        }

        if ($this->successMessage) {
            $array['successMessage'] = $this->successMessage;
        }

        return $array;
    }

    /**
     * @param string|null $redirect
     */
    public function setRedirect(?string $redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @param string|null $successMessage
     */
    public function setSuccessMessage(?string $successMessage): void
    {
        $this->successMessage = $successMessage;
    }

    protected abstract function getFormValues($request);

    protected function getFormData($request)
    {
        return [];
    }
}
