<?php

use Molitor\Admin\Http\Controllers\Admin\AdminController;

Route::middleware('web')->group(
    function () {
        Route::get('/admin', [AdminController::class, 'index'])
            ->middleware('permission:admin')
            ->name('admin');

        Route::get('/admin/settings', [AdminController::class, 'settings'])
            ->middleware('permission:admin')
            ->name('admin.settings');
    }
);
