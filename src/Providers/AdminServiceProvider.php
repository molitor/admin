<?php

namespace Molitor\Admin\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AdminServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'admin');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'admin');

        $this->publishes(
            [
                __DIR__ . '/../resources/vendor' => public_path('vendor'),
            ],
            'admin-public'
        );

        JsonResource::withoutWrapping();
    }

    public function register()
    {
    }
}
