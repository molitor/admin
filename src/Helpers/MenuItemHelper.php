<?php

namespace Molitor\Admin\Helpers;

use Molitor\Menu\Libs\MenuItem;

/**
 * TODO: In the future, all the menu items should have a type property. So, we
 * can use this property to easily distinguish the item type and avoid guessing
 * it by other properties.
 */
class MenuItemHelper
{
    public function itemsToArray(array $items): array
    {
        $array = [];
        foreach ($items as $item) {
            $array[] = $this->itemToArray($item);
        }
        return $array;
    }

    public function itemToArray(MenuItem $item): array
    {

        $menu = [
            'url' => $item->getHref(),
            'text' => $item->getLabel(),
            'type' => '',
            'class' => '',
            'href' => $item->getHref(),
        ];

        $icon = $item->getIcon();
        if($icon) {
            $menu['icon'] = 'fas fa-' . $icon;
        }

        $submenu = $this->itemsToArray($item->getMenuItems());
        if(count($submenu)) {
            $menu['submenu'] = $submenu;
            $menu['submenu_class'] = '';
        }

        return $menu;
    }

    /**
     * Checks if a menu item is a header.
     *
     * @param  mixed  $item
     * @return bool
     */
    public static function isHeader($item)
    {
        return is_string($item) || isset($item['header']);
    }

    /**
     * Checks if a menu item is a link.
     *
     * @param  mixed  $item
     * @return bool
     */
    public static function isLink($item)
    {
        return isset($item['text'])
            && (isset($item['url']) || isset($item['route']));
    }

    /**
     * Checks if a menu item is a submenu.
     *
     * @param  mixed  $item
     * @return bool
     */
    public static function isSubmenu($item)
    {
        return isset($item['text'], $item['submenu'])
            && is_array($item['submenu']);
    }

    /**
     * Checks if a menu item is a legacy search box.
     *
     * @param  mixed  $item
     * @return bool
     */
    public static function isLegacySearch($item)
    {
        return isset($item['text'], $item['search'])
            && ! empty($item['search']);
    }

    /**
     * Checks if a menu item is allowed to be shown (not restricted).
     *
     * @param  mixed  $item
     * @return bool
     */
    public static function isAllowed($item)
    {
        // We won't allow empty submenu items on the menu.

        if (self::isSubmenu($item) && ! count($item['submenu'])) {
            return false;
        }

        // In any other case, fallback to the restricted property.

        return $item && empty($item['restricted']);
    }
}
