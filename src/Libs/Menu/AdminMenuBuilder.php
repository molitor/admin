<?php

namespace Molitor\Admin\Libs\Menu;

use Molitor\Menu\Libs\Menu;
use Molitor\Menu\Libs\MenuBuilder;

class AdminMenuBuilder extends MenuBuilder
{
    public function admin(Menu $menu)
    {
        $menu->addItem('Beállítások', route('admin.settings'))->setIcon('gear');
    }
}
