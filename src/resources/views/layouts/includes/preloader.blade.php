<div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{ asset('images/logo.png') }}" alt="{{ config('app.name') }}" height="60">
</div>
