@php $menu = Menu::admin(); @endphp

<div class="bg-dark border-right" id="sidebar-wrapper">
    <div class="sidebar-heading text-white">
        <a href="{{ route('admin') }}">{{ config('app.name') }}</a>
    </div>
    <ul class="sidebar-nav" data-coreui="navigation" data-simplebar>
        @foreach($menu->getMenuItems() as $item)
            @if($item->getNumMenuItems())
                <li class="nav-group text-white">
                    <a class="nav-link nav-group-toggle text-white" href="{{ $item->getHref() }}"><i class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</a>
                    <ul class="nav-group-items compact">
                        @foreach($item->getMenuItems() as $subItem)
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ $subItem->getHref() }}"><i class="bi bi-{{ $subItem->getIcon() }}"></i> {{ $subItem->getLabel() }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ $item->getHref() }}"><i class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</a>
                </li>
            @endif
        @endforeach
    </ul>
</div>