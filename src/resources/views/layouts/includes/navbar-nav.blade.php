<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
    @foreach($menu->getMenuItems() as $item)
        <li class="nav-item d-none d-sm-inline-block">
            @if($item->isActive())
                <a href="{{ $item->getHref() }}" class="nav-link active"><b><i
                        class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</b></a>
            @else
                <a href="{{ $item->getHref() }}" class="nav-link"><i
                        class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</a>
            @endif
        </li>
    @endforeach
</ul>
