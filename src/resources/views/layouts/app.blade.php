@extends('admin::layouts.base')

@inject('layoutHelper', 'Molitor\Admin\Helpers\LayoutHelper')
@inject('preloaderHelper', 'Molitor\Admin\Helpers\PreloaderHelper')

@section('adminlte_css')
    @stack('css')
    @yield('css')
@stop

@section('classes_body', $layoutHelper->makeBodyClasses())

@section('body_data', $layoutHelper->makeBodyData())

@section('content_header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <div class="col-sm-6">
            <div class="float-sm-right">
                @yield('breadcrumb')
            </div>
        </div>
    </div>
@stop

@section('body')
    <div class="wrapper">

        {{-- Preloader Animation (fullscreen mode) --}}
        @if($preloaderHelper->isPreloaderEnabled())
            @include('admin::partials.common.preloader')
        @endif

        {{-- Top Navbar --}}
        @if($layoutHelper->isLayoutTopnavEnabled())
            @include('admin::partials.navbar.navbar-layout-topnav')
        @else
            @include('admin::partials.navbar.navbar')
        @endif

        {{-- Left Main Sidebar --}}
        @if(!$layoutHelper->isLayoutTopnavEnabled())
            @include('admin::partials.sidebar.left-sidebar')
        @endif

        {{-- Content Wrapper --}}
        @empty($iFrameEnabled)
            @include('admin::partials.cwrapper.cwrapper-default')
        @else
            @include('admin::partials.cwrapper.cwrapper-iframe')
        @endempty

        {{-- Footer --}}
        @hasSection('footer')
            @include('admin::partials.footer.footer')
        @endif

        {{-- Right Control Sidebar --}}
        @if(config('admin.right_sidebar'))
            @include('admin::partials.sidebar.right-sidebar')
        @endif

    </div>
@stop

@section('adminlte_js')
    @stack('js')
    @yield('js')
@stop