@if($menu->getNumMenuItems())
    <ul class="nav nav-pills mb-3">
        @foreach($menu->getMenuItems() as $item)
            @if($item->getNumMenuItems())
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ $item->getHref() }}"
                       role="button"
                       aria-haspopup="true"
                       aria-expanded="false">{{ $item->getLabel() }}</a>
                    <div class="dropdown-menu">
                        @foreach($item->getMenuItems() as $subItem)
                            <a class="dropdown-item" href="{{ $subItem->getHref() }}">
                                <i class=" bi bi-{{ $subItem->getIcon() }}"></i> {{ $subItem->getLabel() }}</a>
                        @endforeach
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link @if($item->isActive()) active @endif" href="{{ $item->getHref() }}">
                        @if($item->getIcon())
                            <i class=" bi bi-{{ $item->getIcon() }}"></i>
                        @endif{{ $item->getLabel() }}</a>
                </li>
            @endif
        @endforeach
    </ul>
@endif
