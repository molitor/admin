@if($menu->getNumMenuItems())
    <ul class="navbar-nav">
        @foreach($menu->getMenuItems() as $item)
            <li class="nav-item d-none d-sm-inline-block">
                @if($item->isActive())
                    <a href="{{ $item->getHref() }}" class="nav-link active"><b><i
                                    class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</b></a>
                @else
                    <a href="{{ $item->getHref() }}" class="nav-link"><i
                                class="bi bi-{{ $item->getIcon() }}"></i> {{ $item->getLabel() }}</a>
                @endif
            </li>
        @endforeach
    </ul>
@endif