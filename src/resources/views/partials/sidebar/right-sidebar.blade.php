<aside class="control-sidebar control-sidebar-{{ config('admin.right_sidebar_theme') }}">
    @yield('right-sidebar')
</aside>
