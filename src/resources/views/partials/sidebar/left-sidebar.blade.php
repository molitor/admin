@inject('menuItemHelper', 'Molitor\Admin\Helpers\MenuItemHelper')

<aside class="main-sidebar {{ config('admin.classes_sidebar', 'sidebar-dark-primary elevation-4') }}">

    {{-- Sidebar brand logo --}}
    @if(config('admin.logo_img_xl'))
        @include('admin::partials.common.brand-logo-xl')
    @else
        @include('admin::partials.common.brand-logo-xs')
    @endif

    {{-- Sidebar menu --}}
    <div class="sidebar">
        <nav class="pt-2">
            <ul class="nav nav-pills nav-sidebar flex-column {{ config('admin.classes_sidebar_nav', '') }}"
                data-widget="treeview" role="menu"
                @if(config('admin.sidebar_nav_animation_speed') != 300)
                    data-animation-speed="{{ config('admin.sidebar_nav_animation_speed') }}"
                @endif
                @if(!config('admin.sidebar_nav_accordion'))
                    data-accordion="false"
                @endif>
                {{-- Configured sidebar links --}}

                @each('admin::partials.sidebar.menu-item', $menuItemHelper->itemsToArray(Menu::admin()->getMenuItems()), 'item')
            </ul>
        </nav>
    </div>

</aside>
