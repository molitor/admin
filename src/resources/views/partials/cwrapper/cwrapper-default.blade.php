@inject('layoutHelper', 'Molitor\Admin\Helpers\LayoutHelper')
@inject('preloaderHelper', 'Molitor\Admin\Helpers\preloaderHelper')

@if($layoutHelper->isLayoutTopnavEnabled())
    @php( $def_container_class = 'container' )
@else
    @php( $def_container_class = 'container-fluid' )
@endif

{{-- Default Content Wrapper --}}
<div class="{{ $layoutHelper->makeContentWrapperClasses() }}">

    {{-- Preloader Animation (cwrapper mode) --}}
    @if($preloaderHelper->isPreloaderEnabled('cwrapper'))
        @include('admin::partials.common.preloader')
    @endif

    {{-- Content Header --}}
    @hasSection('content_header')
        <div class="content-header">
            <div class="{{ config('admin.classes_content_header') ?: $def_container_class }}">
                @yield('content_header')
            </div>
        </div>
    @endif

    {{-- Main Content --}}
    <div class="content">
        @hasSection('actions')
            <div class="{{ config('admin.classes_content_header') ?: $def_container_class }}">
                @yield('actions')
            </div>
        @endif

        <div class="{{ config('admin.classes_content') ?: $def_container_class }}">
            @yield('content')
        </div>
    </div>

</div>
