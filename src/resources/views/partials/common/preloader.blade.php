@inject('preloaderHelper', 'Molitor\Admin\Helpers\PreloaderHelper')

<div class="{{ $preloaderHelper->makePreloaderClasses() }}" style="{{ $preloaderHelper->makePreloaderStyle() }}">

    @hasSection('preloader')

        {{-- Use a custom preloader content --}}
        @yield('preloader')

    @else

        {{-- Use the default preloader content --}}
        <img src="{{ asset(config('admin.preloader.img.path', 'vendor/adminlte/dist/img/AdminLTELogo.png')) }}"
             class="img-circle {{ config('admin.preloader.img.effect', 'animation__shake') }}"
             alt="{{ config('admin.preloader.img.alt', 'AdminLTE Preloader Image') }}"
             width="{{ config('admin.preloader.img.width', 60) }}"
             height="{{ config('admin.preloader.img.height', 60) }}"
             style="animation-iteration-count:infinite;">

    @endif

</div>
