@inject('layoutHelper', 'Molitor\Admin\Helpers\LayoutHelper')

@php( $dashboard_url = View::getSection('dashboard_url') ?? config('admin.dashboard_url', 'admin') )

@if (config('admin.use_route_url', false))
    @php( $dashboard_url = $dashboard_url ? route($dashboard_url) : '' )
@else
    @php( $dashboard_url = $dashboard_url ? url($dashboard_url) : '' )
@endif

<a href="{{ $dashboard_url }}"
    @if($layoutHelper->isLayoutTopnavEnabled())
        class="navbar-brand logo-switch {{ config('admin.classes_brand') }}"
    @else
        class="brand-link logo-switch {{ config('admin.classes_brand') }}"
    @endif>

    {{-- Small brand logo --}}
    <img src="{{ asset(config('admin.logo_img', 'vendor/adminlte/dist/img/AdminLTELogo.png')) }}"
         alt="{{ config('admin.logo_img_alt', 'AdminLTE') }}"
         class="{{ config('admin.logo_img_class', 'brand-image-xl') }} logo-xs">

    {{-- Large brand logo --}}
    <img src="{{ asset(config('admin.logo_img_xl')) }}"
         alt="{{ config('admin.logo_img_alt', 'AdminLTE') }}"
         class="{{ config('admin.logo_img_xl_class', 'brand-image-xs') }} logo-xl">

</a>
