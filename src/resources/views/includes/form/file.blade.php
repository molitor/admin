<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <input type="file" class="form-control" name="{{ $name }}" id="{{ $name }}">
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
