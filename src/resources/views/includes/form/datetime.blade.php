<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <input type="datetime-local" class="form-control" step="1" value="{{ old($name, (isset($value) ? $value : '')) }}"
           name="{{ $name }}"/>
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
