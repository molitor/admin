@php $value = isset($value) ? $value : ''; @endphp
<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <select name="{{ $name }}" id="{{ $name }}" class="form-control">
        @if(isset($required) && $required === false)
            <option value="null">-</option>
        @endif
        @foreach ($options as $key => $option)
            <option value="{{ $key }}"
                    @if ($key == $value) selected="selected" @endif >{{ $option }}</option> @endforeach </select>
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
