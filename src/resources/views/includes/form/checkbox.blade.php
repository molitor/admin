<div class="form-check">
    <input class="form-check-input" type="checkbox" id="{{ $name }}" name="{{ $name }}"
        {{ old($name, isset($value) ? $value : false) ? 'checked="checked"' : '' }}>
    <label class="form-check-label" for="{{ $name }}">
        {{ $label }}
    </label>
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
