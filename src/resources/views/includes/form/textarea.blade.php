<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <textarea name="{{ $name }}" id="{{ $name }}" class="form-control">{{ old($name, isset($value) ? $value: '') }}</textarea>
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
