<form method="POST" action="{{ $href }}">
    @csrf
    @method('delete')
    <button type="submit" class="btn btn-danger btn-icon">
        <i class="fas fa-trash-alt"></i> {{ $label }}
    </button>
</form>
