<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <input type="password" class="form-control" name="{{ $name }}" id="{{ $name }}">
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
