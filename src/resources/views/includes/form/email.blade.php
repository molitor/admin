<div class="form-group">
    @include('admin::includes.form.label', ['for' => $name, 'label' => $label])
    <input type="email" class="form-control" name="{{ $name }}" id="{{ $name }}" value="{{ old($name, isset($value) ? $value: '') }}">
    @include('admin::includes.form.error-message', ['name' => $name])
</div>
