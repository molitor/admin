<div id="delete-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-danger">
                    <i class="fas fa-exclamation-triangle"></i> {{ __('admin::admin.alert') }}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="confirm-modal-message font-weight-bolder"></div>
            </div>
            <div class="modal-footer">
                <form method="POST" action="#" style="display: inline" class="delete-modal-form">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash-alt"></i> {{ __('app.delete') }}
                    </button>
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('app.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
