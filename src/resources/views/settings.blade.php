@extends('admin::layouts.app')

@section('title')
    Beállítások
@endsection

@section('content')
    @if($menu->getNumMenuItems())
        <div class="row">
            @foreach($menu->getMenuItems() as $item)
                <div class="col-3">
                    <a href="{{ $item->getHref() }}"
                       class="btn btn-outline-primary btn-lg btn-block  pt-4 pb-4 @if($item->isActive()) active @endif">
                        <div class="h1"><i class="fas fa-{{ $item->getIcon() }}"></i></div>
                        <div>{{ $item->getLabel() }}</div>
                    </a>
                </div>
            @endforeach
        </div>
    @endif
@endsection
