import FormFieldContainerMixin from "./FormFieldContainerMixin";

export default {
    mixins: [FormFieldContainerMixin],
    data: function () {
        return {
            successMessage: '',
        };
    },
    methods: {
        /*Űrlap értékek elküldése******************************************************************/

        /***
         /**
         * POST metódussal küldi a form értékeit.
         * @param url
         */
        postFormValues(url) {
            return this.callPost(url, this.formValues);
        },
        /**
         * PUT metódussal küldi a form értékeit.
         * @param url
         */
        putFormValues(url) {
            return this.callPut(url, this.formValues);
        },

        /*Végpontok hívása******************************************************************/

        /**
         * GET végpontot hív és beállítja az űrlapot
         * @param url
         */
        callGet(url) {
            this.loading = true;
            this.resetErrors();
            return axios.get(url).then(response => {
                this.handleResponse(response);
            }).catch(error => {
                this.handleErrors(error);
            });
        },
        /**
         * POST végpontot hív és beállítja az űrlapot.
         * @param url
         * @param data
         */
        callPost(url, data) {
            this.loading = true;
            this.resetErrors();
            return axios.post(url, data).then(response => {
                this.handleResponse(response);
            }).catch(error => {
                this.handleErrors(error);
            });
        },
        /**
         * PUT végpontot hív és beállítja az űrlapot.
         * @param url
         * @param data
         */
        callPut(url, data) {
            this.loading = true;
            this.resetErrors();
            return axios.put(url, data).then(response => {
                this.handleResponse(response);
            }).catch(error => {
                this.handleErrors(error);
            });
        },

        /*Visszatérő értékek kezelése******************************************************************/

        /**
         * A visszatérő JSON értékeket kezeli.
         * @param resource
         */
        handleResponse(resource) {
            this.resetErrors();
            if (resource.data.formValues) {
                this.setFormValues(resource.data.formValues);
            }
            if (resource.data.formData) {
                this.setAllFromData(resource.data.formData);
            }
            if (resource.data.redirect) {
                if (resource.data.successMessage) {
                    //sessionStorage.setItem('successMessage', resource.data.successMessage);
                }
                window.location.href = resource.data.redirect;
            } else {
                if (resource.data.successMessage) {
                    this.successMessage = resource.data.successMessage;
                }
            }
            this.showForm = true;
            this.loading = false;
        },

        /**
         * Hiba kezelése
         * @param error
         */
        handleErrors(error) {
            if (error.response.status === 422) {
                this.formErrors = error.response.data.errors;
            }
            this.showForm = true;
            this.loading = false;
        }
    }
}
