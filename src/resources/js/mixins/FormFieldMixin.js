export default {
    props: {
        name: {
            type: String,
            require: true
        },
        label: {
            type: String,
            require: false,
            default: null
        },
        disabled: {
            type: Boolean,
            require: false,
            default: false
        },
        defaultValue: {
            default: ''
        }
    },
    mounted() {
        if (!this.hasParentFormValue(this.name)) {
            this.setParentFormValue(this.name, this.defaultValue);
        }
    },
    methods: {
        hasParentFormValue(name) {
            return this.$parent && this.$parent.formValues.hasOwnProperty(name);
        },
        getParentFormValue(name) {
            if (this.hasParentFormValue(name)) {
                return this.$parent && this.$parent.formValues[name];
            }
            return null;
        },
        setParentFormValue(name, value) {
            this.$parent.formValues[name] = value;
        },
        hasParentFormData(name) {
            return this.$parent && this.$parent.formData.hasOwnProperty(name);
        },
        getParentFormData(name) {
            if (this.hasParentFormData(name)) {
                return this.$parent && this.$parent.formData[name];
            }
            return null;
        },
        setParentFormData(name, value) {
            this.$parent.formData[name] = value;
        }
    },
    computed: {
        isDisabled() {
            if (this.disabled) {
                return true;
            }

            if (this.$parent.loading) {
                return true;
            }

            return false;
        },
        hasError() {
            return this.$parent.hasError(this.name);
        },
        errorMessages() {
            return this.$parent.getErrorMessages(this.name);
        },
        state() {
            return null;
        },
        formValue() {
            return this.getParentFormValue(this.name);
        }
    }
}
