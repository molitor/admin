export default {
    props: {
        values: {
            type: Object,
            require: false,
            default: null
        },
        initData: {
            type: Object,
            require: false,
            default: null
        }
    },
    data: function () {
        return {
            showForm: false,
            loading: true,
            formValues: {},
            formData: {},
            formErrors: []
        };
    },
    methods: {

        /*Alapértelmezett beállítás******************************************************************/

        /**
         * Űrlap alapértelvezett beállítása tulajdonságok alapján
         */
        initFromProps() {
            if (this.values !== null) {
                this.setFormValues(this.values);
            }
            if (this.initData) {
                this.setAllFromData(this.initData);
            }
        },

        /*Mezők beálítása******************************************************************/

        setFormValues(formValues) {
            this.formValues = formValues;
        },

        setFormValue(name, value) {
            this.formValues[name] = value;
        },

        /*Form másodlagos adatok kezelése******************************************************************/

        setAllFromData(formData) {
            this.formData = formData;
        },

        hasFromData(name) {
            return this.formData.hasOwnProperty(name);
        },

        getFromData(name) {
            if (!this.hasFromData(name)) {
                return null;
            }
            return this.formData[name];
        },

        /*Hiba kezelése******************************************************************/

        hasError(name) {
            return this.formErrors.hasOwnProperty(name);
        },

        getState(name) {
            if (this.hasError(name)) {
                return false;
            }
            return null;
        },

        getErrorMessages(name) {
            if (!this.hasError(name)) {
                return [];
            }
            return this.formErrors[name];
        },

        getErrorMessage(name) {
            if (this.formErrors.hasOwnProperty(name)) {
                return this.formErrors[name][0];
            }
            return '';
        },

        resetErrors() {
            this.formErrors = [];
        }
    }
}
