import DestroyModal from "./components/DestroyModal.vue";
import PageSuccessMessage from "./components/PageSuccessMessage.vue";

export {
    DestroyModal,
    PageSuccessMessage
}

/*

//Admin ====================================================================

import {
    DestroyModal,
    PageSuccessMessage
} from '../../vendor/molitor/admin/src/resources/js/app.js';

app.component('destroy-modal', DestroyModal);
app.component('page-success-message', PageSuccessMessage);

*/
